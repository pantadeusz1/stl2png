/**
 * @file stl2png.cpp
 * @author Tadeusz Puźniakowski
 * @brief This is verry simple converter for generating png heightmaps from STL 3D files
 * @version 0.1
 * @date 2022-02-01
 * 
 * @copyright Copyright (c) 2022
 * 
 */

// methods to convert stl into image
#include "lodepng.h"
#include <array>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

using point3d_t = std::array<double, 3>;

class image_t : public std::vector<unsigned char>
{
public:
    int width;
    int height;

    void put_pixel(int x, int y, unsigned char c)
    {
        this->operator[](y * (width) + x) = c;
    };

    unsigned char& getPixel(int x, int y)
    {
        return this->operator[](y * width + x);
    };

    image_t() : std::vector<unsigned char>()
    {
        width = 0;
        height = 0;
    }
    image_t(int width_, int height_) : std::vector<unsigned char>(width_ * height_), width(width_), height(height_)
    {
    }

    void save(std::string fname)
    {
        std::vector<unsigned char> bytes;
        for (unsigned i = 0; i < size(); i++) {
            bytes.push_back((*this)[i]);
            bytes.push_back((*this)[i]);
            bytes.push_back((*this)[i]);
            bytes.push_back(255);
        }
        lodepng::encode(fname, bytes, width, height);
    }
};

point3d_t rotateAxisZ(const point3d_t& p, double q)
{
    point3d_t np = {
        p[0] * std::cos(q) - p[1] * std::sin(q),
        p[0] * std::sin(q) + p[1] * std::cos(q),
        p[2]};
    return np;
};
// p: FVertex, q: number
point3d_t rotateAxisY(const point3d_t& p, double q)
{
    point3d_t np = {
        p[1] * std::cos(q) - p[2] * std::sin(q),
        p[1] * std::sin(q) + p[2] * std::cos(q),
        p[0]};
    return np;
};
// p: FVertex, q: number
point3d_t rotateAxisX(const point3d_t& p, double q)
{
    point3d_t np = {
        p[2] * std::cos(q) - p[0] * std::sin(q),
        p[2] * std::sin(q) + p[0] * std::cos(q),
        p[1]};
    return np;
};


// p: FVertex, v: FVertex[]
double get_z_value_on_triangle(std::array<double, 2> p, std::array<point3d_t, 3> v)
{
    auto x = p[0];
    auto y = p[1];
    // auto tmpZ = p[2];
    auto e0 =
        (x * (v[0][1] * (v[1][2] - v[2][2]) + v[0][2] * (v[2][1] - v[1][1]) +
                 v[1][1] * v[2][2] - v[1][2] * v[2][1]));
    auto e1 =
        (v[0][0] * (v[2][1] - v[1][1]) + v[0][1] * (v[1][0] - v[2][0]) -
            v[1][0] * v[2][1] + v[1][1] * v[2][0]);
    auto e2 =
        (y * (-v[0][0] * v[1][2] + v[0][0] * v[2][2] + v[0][2] * v[1][0] -
                 v[0][2] * v[2][0] - v[1][0] * v[2][2] + v[1][2] * v[2][0]));
    auto e3 =
        (-v[0][0] * v[1][1] + v[0][0] * v[2][1] + v[0][1] * v[1][0] -
            v[0][1] * v[2][0] - v[1][0] * v[2][1] + v[1][1] * v[2][0]);
    auto e4 =
        (-v[0][0] * v[1][1] * v[2][2] + v[0][0] * v[1][2] * v[2][1] +
            v[0][1] * v[1][0] * v[2][2] - v[0][1] * v[1][2] * v[2][0] -
            v[0][2] * v[1][0] * v[2][1] + v[0][2] * v[1][1] * v[2][0]);
    auto e5 =
        (-v[0][0] * v[1][1] + v[0][0] * v[2][1] + v[0][1] * v[1][0] -
            v[0][1] * v[2][0] - v[1][0] * v[2][1] + v[1][1] * v[2][0]);
    auto z = e0 / e1 + e2 / e3 + e4 / e5;
    return z;
};


/*

the method for rasterizing is taken from (and modified)
http://www.sunshine2k.de/coding/java/TriangleRasterization/TriangleRasterization.html#algo2
*/
//    img: ImageData, T: FVertex[], color: number[],fmaxmin
void draw_triangle_to_image(
    image_t& img,
    std::array<point3d_t, 3> T,
    std::function<double(double, double)> f = [](auto a, auto b) { return std::max(a, b); })
{
    // following http://forum.devmaster.net/t/advanced-rasterization/6145
    // 28.4 fixed-point coordinates
    int Y1 = 16.0 * T[0][1];
    int Y2 = 16.0 * T[1][1];
    int Y3 = 16.0 * T[2][1];
    int X1 = 16.0 * T[0][0];
    int X2 = 16.0 * T[1][0];
    int X3 = 16.0 * T[2][0];

    // Deltas
    auto DX12 = X1 - X2;
    auto DX23 = X2 - X3;
    auto DX31 = X3 - X1;

    auto DY12 = Y1 - Y2;
    auto DY23 = Y2 - Y3;
    auto DY31 = Y3 - Y1;

    // Fixed-point deltas
    auto FDX12 = DX12 << 4;
    auto FDX23 = DX23 << 4;
    auto FDX31 = DX31 << 4;

    auto FDY12 = DY12 << 4;
    auto FDY23 = DY23 << 4;
    auto FDY31 = DY31 << 4;

    // Bounding rectangle
    auto minx = (std::min(X1, std::min(X2, X3)) + 0xF) >> 4;
    auto maxx = (std::max(X1, std::max(X2, X3)) + 0xF) >> 4;
    auto miny = (std::min(Y1, std::min(Y2, Y3)) + 0xF) >> 4;
    auto maxy = (std::max(Y1, std::max(Y2, Y3)) + 0xF) >> 4;

    auto currentY = miny;

    // Half-edge autoants
    auto C1 = DY12 * X1 - DX12 * Y1;
    auto C2 = DY23 * X2 - DX23 * Y2;
    auto C3 = DY31 * X3 - DX31 * Y3;

    // Correct for fill convention
    if ((DY12 < 0) || (DY12 == 0 && DX12 > 0)) {
        C1++;
    }
    if ((DY23 < 0) || (DY23 == 0 && DX23 > 0)) {
        C2++;
    }
    if ((DY31) < 0 || (DY31 == 0 && DX31 > 0)) {
        C3++;
    }

    auto CY1 = C1 + DX12 * (miny << 4) - DY12 * (minx << 4);
    auto CY2 = C2 + DX23 * (miny << 4) - DY23 * (minx << 4);
    auto CY3 = C3 + DX31 * (miny << 4) - DY31 * (minx << 4);


    for (auto y = miny; y < maxy; y++) {
        auto CX1 = CY1;
        auto CX2 = CY2;
        auto CX3 = CY3;

        for (auto x = minx; x < maxx; x++) {
            if (CX1 > 0 && CX2 > 0 && CX3 > 0) {
                img.put_pixel(x, currentY,
                    f(img.getPixel(x, currentY),
                        std::max(std::min((int)get_z_value_on_triangle({(double)x, (double)currentY}, T), 255), 0)));
            }

            CX1 -= FDY12;
            CX2 -= FDY23;
            CX3 -= FDY31;
        }

        CY1 += FDX12;
        CY2 += FDX23;
        CY3 += FDX31;

        currentY++;
    }
}

/*
STL format

UINT8[80]    – Header                 -     80 bytes
UINT32       – Number of triangles    -      4 bytes

foreach triangle                      - 50 bytes:
    REAL32[3] – Normal vector             - 12 bytes
    REAL32[3] – Vertex 1                  - 12 bytes
    REAL32[3] – Vertex 2                  - 12 bytes
    REAL32[3] – Vertex 3                  - 12 bytes
    UINT16    – Attribute byte count      -  2 bytes
end
*/
class triangle_t
{
public:
    point3d_t normal;
    std::array<point3d_t, 3> v;
    std::array<unsigned char, 2> attribute;

    triangle_t(std::vector<unsigned char> data)
    {
        normal[0] = *(float*)(data.data() + 0);
        normal[1] = *(float*)(data.data() + 4);
        normal[2] = *(float*)(data.data() + 8);

        for (int idx = 0; idx < 3; idx++) {
            v[idx][0] = *(float*)(data.data() + 12 + idx * 12);
            v[idx][1] = *(float*)(data.data() + 12 + idx * 12 + 4);
            v[idx][2] = *(float*)(data.data() + 12 + idx * 12 + 8);
        }
        attribute[0] = *(data.data() + 48);
        attribute[1] = *(data.data() + 49);
    }
    triangle_t(){};
};

class stl_t
{
public:
    std::vector<unsigned char> header;
    std::vector<triangle_t> triangles;

    /**
     * @brief Construct a new stl t object using binary data from stl file
     *
     * @param data
     */
    stl_t(std::vector<unsigned char> data)
    {
        header = {data.begin(), data.begin() + 80};
        triangles = std::vector<triangle_t>(*(uint32_t*)(data.data() + 80));
        for (unsigned int i = 0; i < triangles.size(); i++) {
            triangles[i] = triangle_t({data.begin() + 84 + (i * 50), data.begin() + 84 + ((i + 1) * 50)});
        }
    }

    std::pair<point3d_t, point3d_t> find_limits() const
    {
        point3d_t minimums = {10000, 10000, 10000};
        point3d_t maximums = {-10000, -10000, -10000};
        for (auto& face : triangles) {
            for (auto& v : face.v) {
                for (int i = 0; i < 3; i++) {
                    minimums[i] = std::min(v[i], minimums[i]);
                    maximums[i] = std::max(v[i], maximums[i]);
                }
            }
        }
        return {minimums, maximums};
    }
};


// p: FVertex, q: number
// stl3D: Stl3D, dpi: number, rotation: FVertex
/**
 * @brief draws stl image into grayscale bitmap. The closer to the top, the brighter the pixels are
 *
 * @param stl3D stl file after parsing
 * @param dpi dots per inch - the resolution of the result image
 * @param rotation rotation along axis. It is applied in the following order: z->y->x
 * @param margin the empty space around object in [mm]
 * @return image_t the resulting image that can be saved on disk
 */
image_t draw_stl_image_as_2d_depthmap(stl_t stl3D, double dpi, point3d_t rotation, double margin = 2.0)
{
    for (auto& face : stl3D.triangles) {
        for (auto& v0 : face.v) {
            auto v = rotateAxisX(rotateAxisY(rotateAxisZ(v0, rotation[2]), rotation[1]), rotation[0]);
            v0 = v;
        }
        face.normal = rotateAxisX(rotateAxisY(rotateAxisZ(face.normal, rotation[2]), rotation[1]), rotation[0]);
    }

    auto [minimums, maximums] = stl3D.find_limits();
    auto scaleXY = dpi * 0.03937008;
    auto shiftX = scaleXY * (margin - minimums[0]);
    auto shiftY = scaleXY * (margin + maximums[1]);
    int image_width = scaleXY * (maximums[0] + margin * 2 - minimums[0]);
    int image_height = scaleXY * (maximums[1] + margin * 2 - minimums[1]);

    auto scaleZ = 1.0 / (maximums[2] - minimums[2]);
    auto shiftZ = -minimums[2] * scaleZ;

    image_t imdata(image_width, image_height);

    auto fx = [&](auto x) -> double { return x * scaleXY + shiftX; };
    auto fy = [&](auto y) -> double { return (-y) * scaleXY + shiftY; };
    auto fz = [&](auto z) -> double { return z * scaleZ + shiftZ; };

    for (auto& triangle : stl3D.triangles) {
        for (auto& e : triangle.v) {
            e = {fx(e[0]), fy(e[1]), fz(e[2]) * 256.0f};
        }
    }

    for (auto triangle : stl3D.triangles) {
        draw_triangle_to_image(imdata, triangle.v);
    }
    return imdata;
}

int main(int argc, char** argv)
{
    std::vector<std::string> args(argv, argv + argc);
    try {
        // open the file:
        std::ifstream file(args.at(1), std::ios::binary);

        // read the data:
        std::vector<unsigned char> stl_data((std::istreambuf_iterator<char>(file)),
            std::istreambuf_iterator<char>());

        if (stl_data.size() < 88) throw std::invalid_argument("you must provide valid binary STL file");
        stl_t stl(stl_data);

        auto image = draw_stl_image_as_2d_depthmap(stl, 300,
            {(args.size() > 3) ? std::stof(args[3]) : 0.0f,
                (args.size() > 4) ? std::stof(args[4]) : 0.0f,
                (args.size() > 5) ? std::stof(args[5]) : 0.0f},
            2.0f);
        image.save(args.at(2));
        return 0;
    } catch (const std::exception& e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
        std::cerr << "Running: " << std::endl;
        std::cerr << args.at(0) << " input_file.stl output_file.png rotationX rotationY rotationZ" << std::endl;
    }
}
