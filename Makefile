
BUILDDIR=build


ARCH=$(shell uname -i)
INCLUDE=-Wno-psabi -Wall -I${THIRDPARTYDIR}/distance_t -I${THIRDPARTYDIR}/json
CFLAGS= -std=c++17 -DARCH_${ARCH} -fopenmp -O3 ${INCLUDE}
LIBS=-fopenmp

ARCH=$(shell uname -i)

OBJFILES= build/stl2png.o build/lodepng.o

all: build/stl2png

build/stl2png: ${OBJFILES}
	g++ $(LIBS) $^ -o $@

build/%.o : src/%.cpp
	g++ ${CFLAGS} -c $< -o $@

clean:
	rm -rf build/stl2png
	rm -rf build/*.o
